package com.epam.rd.java.basic.task7.db;

public abstract class Const {

    public static class Queries{
        public static class User {
            public static final String INSERT       = "INSERT INTO users (login) VALUES(?)";

//            public static final String INSERT       = "INSERT INTO users (login) " +
//                                                      "(SELECT ? " +
//                                                      "FROM users " +
//                                                      "WHERE login = ? " +
//                                                      "HAVING count(*)=0)";
            public static final String SELECT_ALL   = "SELECT * FROM users";
            public static final String DELETE       = "DELETE FROM users WHERE login = ?";
            public static final String SELECT_ONE   = "SELECT * FROM users WHERE login = ?";
        }

        public static class Team {
            public static final String INSERT       = "INSERT INTO teams (name) VALUES(?)";
//            public static final String INSERT       = "INSERT INTO teams (name) " +
//                                                      "(SELECT ? " +
//                                                      "FROM teams " +
//                                                      "WHERE name = ? " +
//                                                      "HAVING count(*)=0)";
            public static final String SELECT_ALL   = "SELECT * FROM teams";
            public static final String DELETE       = "DELETE FROM teams WHERE name = ?";
            public static final String SELECT_ONE   = "SELECT * FROM teams WHERE name = ?";
            public static final String UPDATE       = "UPDATE teams SET name = ? WHERE id = ?";
        }

        public static class UsersTeams {
            public static final String INSERT       = "INSERT INTO users_teams VALUES(" +
                    "(SELECT id FROM users WHERE login = ?), " +
                    "(SELECT id FROM teams WHERE name = ?))";

//            public static final String INSERT = "INSERT INTO users_teams  " +
//                    "(SELECT (SELECT id FROM users WHERE login = ?) as user_id, (SELECT id FROM teams WHERE name = ?) as team_id " +
//                    "FROM users_teams WHERE user_id = (SELECT id FROM users WHERE login = ?) AND  team_id = (SELECT id FROM teams WHERE name = ?) " +
//                    "HAVING count(*)=0)";

//            public static final String INSERT       = "SET @user_id = (SELECT id FROM users WHERE login = ?); " +
//                                                      "SET @team_id = (SELECT id FROM teams WHERE name = ?); " +
//
//                                                      "INSERT INTO users_teams " +
//                                                      "(SELECT @user_id as user_id, @team_id as team_id " +
//                                                      "FROM users_teams WHERE user_id = @user_id AND  team_id = @team_id " +
//                                                      "HAVING count(*)=0);";

            public static final String SELECT_USER_TEAMS
                    = "SELECT * " +
                      "FROM teams " +
                      "INNER JOIN users_teams ON users_teams.team_id = teams.id " +
                      "WHERE users_teams.user_id = ?";
        }
    }
}
