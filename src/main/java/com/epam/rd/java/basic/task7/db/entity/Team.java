package com.epam.rd.java.basic.task7.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);

		return team;
	}

	public static Team createTeam(int id, String name) {
		Team team = new Team();
		team.setName(name);
		team.setId(id);

		return team;
	}


	@Override
	public String toString() {
		return this.getName();
	}

	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (this.getClass() != obj.getClass()) return false;

		Team team = (Team) obj;
		return this.getName().equals(team.getName());
	}
}
