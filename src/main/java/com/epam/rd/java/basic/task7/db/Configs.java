package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.ResourceBundle;

public enum Configs {
    HOST("localhost"),
    PORT("3306"),
    USER("root"),
    PASS("123456"),
    NAME("testdb");

    String config;

    Configs(String config){
        this.config = config;
    }

//    public static String getURL(){
//        return String.format("jdbc:mysql://%1$s:%2$s/%3$s?" +
//                        "user=%4$s&" +
//                        "password=%5$s&" +
//                        "autoReconnect=true&" +
//                        "useSSL=false",
//                Configs.HOST, Configs.PORT,Configs.NAME, Configs.USER, Configs.PASS);
//    }

    public static String getURL(){

        String URL = null;

        try (InputStream input = new FileInputStream("app.properties");) {
            Properties prop = new Properties();

            prop.load(input);

            URL = prop.getProperty("connection.url");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return URL;
    }

    @Override
    public String toString() {
        return this.config;
    }
}
