package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.protocol.Message;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		//hide
	}

	private void close(AutoCloseable... insts) throws DBException{
		try {
			for (AutoCloseable inst : insts) {
				if(inst != null)
					inst.close();
			}
		} catch (Exception e) {
			throw new DBException("CLOSE ERROR", e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		Statement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		List<User> UserList = new ArrayList<>();

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(Const.Queries.User.SELECT_ALL);

			while(rs.next()){
				UserList.add(User.createUser(rs.getInt("id"), rs.getString("login")));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("SELECT ALL USER ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}
		return UserList;
	}

	public boolean insertUser(User user) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.User.INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, user.getLogin());

			stmt.executeUpdate();

			rs = stmt.getGeneratedKeys();

			if(rs.next()){
				user.setId(rs.getInt(1));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("INSERT USER ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}
		return true;
	}


	public boolean deleteUsers(User... users) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		try{
			conn = DriverManager.getConnection(Configs.getURL());
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(Const.Queries.User.DELETE);

			for(User user : users){
				stmt.setString(1, user.getLogin());
				stmt.addBatch();
			}

			stmt.executeBatch();
			conn.commit();
		} catch (SQLException throwables) {
			try {
				conn.rollback();
				throwables.printStackTrace();
				throw new DBException("DELETE USER ERROR", throwables);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}finally {
			close(conn, stmt, rs);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User user;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.User.SELECT_ONE);
			stmt.setString(1, login);
			rs = stmt.executeQuery();
			rs.next();

			user = User.createUser(rs.getInt("id"), rs.getString("login"));

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("SELECT USER ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		Team team;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.Team.SELECT_ONE);
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			rs.next();

			team = Team.createTeam(rs.getInt("id"), rs.getString("name"));

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("SELECT TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Statement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		List<Team> TeamsList = new LinkedList<>();

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(Const.Queries.Team.SELECT_ALL);

			while(rs.next()){
				TeamsList.add(Team.createTeam(rs.getInt("id"), rs.getString("name")));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("SELECT ALL TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}
		return TeamsList;
	}

	public boolean insertTeam(Team team) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.Team.INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, team.getName());
//			stmt.setString(2, team.getName());

			stmt.executeUpdate();

			rs = stmt.getGeneratedKeys();

			if(rs.next()){
				team.setId(rs.getInt(1));
			}

//			System.out.println(team.getName() + " " + team.getId());
//			System.out.println(team.getName());


		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("INSERT TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(Const.Queries.UsersTeams.INSERT);

			for(Team team : teams){
				stmt.setString(1, user.getLogin());
//				stmt.setString(3, user.getLogin());
				stmt.setString(2, team.getName());
//				stmt.setString(4, team.getName());
				stmt.addBatch();
			}

			stmt.executeBatch();
			conn.commit();

		} catch (SQLException throwables){
			try {
				conn.rollback();
				throw new DBException("SET TEAM ERROR", throwables);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}finally {
			close(conn, stmt, rs);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		List<Team> TeamsList = new ArrayList<>();

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.UsersTeams.SELECT_USER_TEAMS);
			stmt.setInt(1, user.getId());
			rs = stmt.executeQuery();

			while(rs.next()){

				TeamsList.add(Team.createTeam(rs.getInt("id"), rs.getString("name")));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("SELECT ALL TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}
		return TeamsList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.Team.DELETE);

			stmt.setString(1, team.getName());
			stmt.execute();

		} catch (SQLException throwables) {
				throwables.printStackTrace();
				throw new DBException("DELETE TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try{
			conn = DriverManager.getConnection(Configs.getURL());
			stmt = conn.prepareStatement(Const.Queries.Team.UPDATE);

			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.execute();

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("UPDATE TEAM ERROR", throwables);
		}finally {
			close(conn, stmt, rs);
		}

		return true;
	}

}
